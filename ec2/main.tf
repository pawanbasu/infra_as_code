provider "aws" {

    region = "us-east-1"
   
}
terraform {
    required_providers {
    aws = {
        source ="hashicorp/aws"
        version = "~>3.75"
    
       
    }
    }
backend "s3" {
        bucket = "dev-applications-backend-state-in-19april2022today"
       # key = "backend-state-users-dev"
        key = "backend-state/ec2/"
        region = "us-east-1"
        dynamodb_table = "dev_application_locks"
        encrypt = true
    }
    }
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

data "aws_subnets" "selected_subnet" {
  filter {
    name   = "vpc-id"
    values = [aws_default_vpc.default.id]
  }
}
resource "aws_instance" "http_server" {
    ami = var.http_server_ami
    subnet_id = tolist(data.aws_subnets.selected_subnet.ids)[0]
    instance_type = var.http_server_instance_type	    
}

resource "aws_instance" "db_server" {
    ami = var.db_server_ami
    subnet_id = tolist(data.aws_subnets.selected_subnet.ids)[0]
    instance_type = var.db_server_instance_type    
}
